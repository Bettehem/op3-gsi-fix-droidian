#!/bin/sh
# Droidian GSI fix script for the OnePlus 3/3T
OUTFD=/proc/self/fd/$1

# ui_print <text>
ui_print() { echo -e "ui_print $1\nui_print" > $OUTFD; }

#Take username from filename
#USERNAME="$(echo "$2" | cut -d- -f4 | cut -d. -f1)"

USERNAME="droidian"

mkdir /tmp/m
mkdir /tmp/a

ui_print ""
ui_print "Resizing rootfs to 16GB"
e2fsck -fy /data/rootfs.img
resize2fs /data/rootfs.img 16G

mount /data/rootfs.img /tmp/m
mount /tmp/m/var/lib/lxc/android/android-rootfs.img /tmp/a

ui_print ""
ui_print "Fixing display brightness setting"
cp -fpr /data/gsi-fix/files/90-backlight.rules /tmp/m/etc/udev/rules.d/
chmod 0666 /tmp/m/etc/udev/rules.d/90-backlight.rules

ui_print ""
ui_print "Fixing display scaling, will reset after phosh update"
PHOSH_CONFIG=/tmp/m/usr/share/phosh/phoc.ini
sed -i 's/scale = 2/scale = 1/g' $PHOSH_CONFIG
sed -i 's/scale = 3/scale = 2/g' $PHOSH_CONFIG

ui_print ""
ui_print "Enabling double tap to wake"
cp -fpr /data/gsi-fix/files/double-tap.service /tmp/m/etc/systemd/system/double-tap.service
chmod 0644 /tmp/m/etc/systemd/system/double-tap.service
mkdir -p /tmp/m/etc/systemd/system/graphical.target.wants
ln -s /etc/systemd/system/double-tap.service /tmp/m/etc/systemd/system/graphical.target.wants/double-tap.service
mkdir -p /tmp/m/etc/systemd/system-preset
cp -fpr /data/gsi-fix/files/10-double-tap.preset /tmp/m/etc/systemd/system/system-preset/
chmod 0644 /tmp/m/etc/systemd/system-preset/10-double-tap.preset

ui_print ""
ui_print "Fixing WiFi"
cp -fpr /data/gsi-fix/files/wifi-fix.service /tmp/m/etc/systemd/system/wifi-fix.service
chmod 0644 /tmp/m/etc/systemd/system/wifi-fix.service
mkdir -p /tmp/m/etc/systemd/system/graphical.target.wants
ln -s /etc/systemd/system/wifi-fix.service /tmp/m/etc/systemd/system/graphical.target.wants/wifi-fix.service
mkdir -p /tmp/m/etc/systemd/system-preset
cp -fpr /data/gsi-fix/files/00-wifi-fix.preset /tmp/m/etc/systemd/system-preset/
chmod 0644 /tmp/m/etc/systemd/system-preset/00-wifi-fix.preset

ui_print ""
ui_print "Fixing Bluetooth"
cp -fpr /data/gsi-fix/files/droid-get-bt-address.sh /tmp/m/usr/bin/droid/droid-get-bt-address.sh

ui_print ""
ui_print "Adding files for the OnePlus3 droidian adaptation"
cp -fpr /data/gsi-fix/files/oneplus3.list /tmp/m/etc/apt/sources.list.d/
cp -fpr /data/gsi-fix/files/oneplus3.gpg /tmp/m/usr/share/keyrings/oneplus3.gpg
ui_print ""
ui_print "VERY IMPORTANT!"
ui_print ""
ui_print "When you have booted the device for the first time, run this command in the terminal:"
ui_print ""
ui_print "sudo apt update && sudo apt install adaptation-droidian-oneplus3"

ui_print ""
ui_print ""
ui_print ""
ui_print "Adding move-home script"
mkdir -p "/tmp/m/home/$USERNAME/.local/bin"
cp -fpr /data/gsi-fix/files/move-home "/tmp/m/home/$USERNAME/.local/bin/"
chmod +x  "/tmp/m/home/$USERNAME/.local/bin/move-home"
echo "PATH=\$PATH:\$HOME/.local/bin" >> "/tmp/m/home/$USERNAME/.bashrc"
ui_print ""
ui_print "After installing adaptation-droidian-oneplus3, run this command in the terminal: move-home"
ui_print ""
ui_print "It will move your home directory to the userdata partition so you can use all of the available storage on the device."
ui_print ""
ui_print "After running move-home, your device will reboot."


ui_print ""
ui_print "Note: When booting into droidian, you will first get the droidian logo, then a black screen for about 20-30 seconds."
ui_print ""
ui_print "When the screen goes black, do not press the power button! The screen will turn back on when the device has finished booting."

umount /tmp/a
umount /tmp/m

## end install
