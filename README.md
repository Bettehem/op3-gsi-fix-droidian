`op3-gsi-fix-droidian` is an installer which does some fixes required for running [Droidian](https://github.com/droidian-images/droidian) on the OnePlus 3, like fixing wifi, display brightness etc.\
It also adds a command: `move-home`. It will move your user's home directory to the devices userdata partition, so you have the full 64/128GB storage available for use.\
\
Note: this will also work with [Manjaro ARM](https://github.com/manjaro-libhybris/image-ci) if you change the value of USERNAME from `droidian` to `manjaro` in gsi-fix.sh

Download:\
Get the latest automated build [here](https://gitlab.com/Bettehem/op3-gsi-fix-droidian/-/jobs/artifacts/main/browse?job=makezip).

Installation & Usage:\
After flashing your droidian.zip, flash op3-gsi-fix-droidian.zip, either using TWRP's own install method or adb sideload.\
When you boot Droidian for the first time, run this command in the terminal: `move-home`. Your device will reboot after its done.\
Enjoy Droidian on your OnePlus 3!

Updating:\
To update to a newer version of op3-gsi-fix-droidian, simply flash the new .zip using TWRP's own install method or adb sideload.\
Do not run the `move-home` command after updating. It should only be used after booting Droidian for the very first time.

