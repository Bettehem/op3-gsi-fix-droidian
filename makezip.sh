#!/bin/sh
# creates a zip file from the files in gsi-fix directory
ZIPNAME=op3-gsi-fix-droidian.zip
if [ -f "$ZIPNAME" ]; then
    echo "Removing old zip"
    rm "$ZIPNAME"
fi
cd gsi-fix || exit
echo "Creating new zip..."
zip -r ../"$ZIPNAME" $(ls) && echo "Done."
cd ..
